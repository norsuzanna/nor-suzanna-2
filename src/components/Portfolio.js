import React, { Component } from "react";
export default class Porfolio extends Component {
  render() {
    let resumeData = this.props.resumeData;
    return (
      <section id="portfolio">
        <div className="row">
          <div className="twelve columns collapsed">
            <h1>Portfolio</h1>
            <div
              id="portfolio-wrapper"
              className="bgrid-quarters s-bgrid-thirds cf"
            >
              {resumeData.portfolio &&
                resumeData.portfolio.map((item, key) => {
                  return (
                    <div className="columns portfolio-item" key={key}>
                      <div className="item-wrap">
                        <img
                          src={`${item.imgurl}`}
                          className="item-img"
                          alt="Portfolio"
                        />
                        <div className="overlay">
                          {item.url !== "" ? (
                            <a href={item.url} target="blank">
                              <div className="portfolio-item-meta">
                                <h5>{item.name}</h5>
                                <p>{item.description}</p>
                                <ul>
                                  {item.app &&
                                    item.app.map((app) => {
                                      return (
                                        <li
                                          key={app.url}
                                          style={{
                                            display: "inline-block",
                                            padding: 0,
                                            margin: "0 14%",
                                          }}
                                        >
                                          <a href={app.url} target="blank">
                                            <i
                                              className={app.icon}
                                              style={{ fontSize: 50 }}
                                            />
                                          </a>
                                        </li>
                                      );
                                    })}
                                </ul>
                              </div>
                            </a>
                          ) : (
                            <div className="portfolio-item-meta">
                              <h5>{item.name}</h5>
                              <p>{item.description}</p>
                              <ul>
                                {item.app &&
                                  item.app.map((app) => {
                                    return (
                                      <li
                                        key={app.url}
                                        style={{
                                          display: "inline-block",
                                          padding: 0,
                                          margin: "0 14%",
                                        }}
                                      >
                                        <a href={app.url} target="blank">
                                          <i
                                            className={app.icon}
                                            style={{ fontSize: 50 }}
                                          />
                                        </a>
                                      </li>
                                    );
                                  })}
                              </ul>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </section>
    );
  }
}
