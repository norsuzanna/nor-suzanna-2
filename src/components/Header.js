import React, { Component } from "react";
export default class Header extends Component {
  render() {
    let resumeData = this.props.resumeData;
    return (
      <React.Fragment>
        <header id="home">
          <nav id="nav-wrap">
            <a className="mobile-btn" href="#nav-wrap" title="Show navigation">
              Show navigation
            </a>
            <a className="mobile-btn" href="#d" title="Hide navigation">
              Hide navigation
            </a>
            <ul id="nav" className="nav">
              <li className="current">
                <a className="smoothscroll" href="#home">
                  Home
                </a>
              </li>
              <li>
                <a className="smoothscroll" href="#about">
                  About
                </a>
              </li>
              <li>
                <a className="smoothscroll" href="#skills">
                  Skills
                </a>
              </li>
              <li>
                <a className="smoothscroll" href="#portfolio">
                  Portfolio
                </a>
              </li>
              {/* <li>
                <a className="smoothscroll" href="#testimonials">
                  Testimonials
                </a>
              </li>
              <li>
                <a className="smoothscroll" href="#contact">
                  Contact
                </a>
              </li> */}
            </ul>
          </nav>
          <div className="row banner">
            <div className="banner-text">
              <h1 className="responsive-headline">{resumeData.name}</h1>
              <h3 style={{ color: "#fff", fontFamily: "sans-serif " }}>
                {/* {resumeData.roleDescription} */}I am a Senior Software
                Developer from{" "}
                <span style={{ background: "#fff" }}>
                  <span style={{ color: "#010066" }}>M</span>
                  <span style={{ color: "#CC0001" }}>a</span>
                  <span style={{ color: "#FFCC00" }}>l</span>
                  <span style={{ color: "#010066" }}>a</span>
                  <span style={{ color: "#CC0001" }}>y</span>
                  <span style={{ color: "#FFCC00" }}>s</span>
                  <span style={{ color: "#010066" }}>i</span>
                  <span style={{ color: "#CC0001" }}>a</span>
                </span>
                . I started my career as Android Developer and slowly jump into
                hybrid mobile and web development. After 10 years in software
                development, I learned (still learning) how to solve problems,
                make code more efficient and inspire other developers.
              </h3>
              <hr />
              <ul className="social">
                {resumeData.socialLinks &&
                  resumeData.socialLinks.map(item => {
                    return (
                      <li key={item.name}>
                        <a href={item.url} target="blank">
                          <i className={item.className}></i>
                        </a>
                      </li>
                    );
                  })}
              </ul>
            </div>
          </div>

          <p className="scrolldown">
            <a className="smoothscroll" href="#about">
              <i className="icon-down-circle"></i>
            </a>
          </p>
        </header>
      </React.Fragment>
    );
  }
}
