import { aia, rhb, myhome } from "./images/portfolio/images";

let resumeData = {
  name: "Nor Suzanna",
  role: "Frontend Developer and Data Scientist",
  linkedinId: "Your LinkedIn Id",
  skypeid: "Your skypeid",
  roleDescription:
    "I like dabbling in various parts of frontend development and like to learn about new technologies, write technical articles or simply play games in my free time.",
  socialLinks: [
    {
      name: "linkedin",
      url: "https://www.linkedin.com/in/norsuzanna/",
      className: "fa fa-linkedin"
    },
    {
      name: "github",
      url: "https://github.com/norsuzanna",
      className: "fa fa-github"
    },
    {
      name: "gitlab",
      url: "https://gitlab.com/norsuzanna",
      className: "fa fa-gitlab"
    },
  ],
  aboutme:
    "10 years experienced Senior Software Engineer with a demonstrated history of working in the fintech, courier service, banking, IT consulting & agri-tech industry. Skilled in web and mobile application development (ReactJS, VueJS, React Native, Angular, Node.js, Laravel, Flutter etc). Strong engineering professional with a Master's Degree focused in Computer Science from Universiti Teknologi MARA.",
  address: "Klang, Selangor, Malaysia",
  age: "33 years old",
  website: "https://rbhatia46.github.io",
  education: [
    {
      UniversityName: "The LNM Insitute of Information Technology",
      specialization: "Some specialization",
      MonthOfPassing: "Aug",
      YearOfPassing: "2020",
      Achievements: "Some Achievements"
    },
    {
      UniversityName: "Some University",
      specialization: "Some specialization",
      MonthOfPassing: "Jan",
      YearOfPassing: "2018",
      Achievements: "Some Achievements"
    }
  ],
  work: [
    {
      CompanyName: "Some Company",
      specialization: "Some specialization",
      MonthOfLeaving: "Jan",
      YearOfLeaving: "2018",
      Achievements: "Some Achievements"
    },
    {
      CompanyName: "Some Company",
      specialization: "Some specialization",
      MonthOfLeaving: "Jan",
      YearOfLeaving: "2018",
      Achievements: "Some Achievements"
    }
  ],
  skillsDescription: "Your skills here",
  skills: [
    {
      skillname: "NEXTJS"
    },
    {
      skillname: "REACTJS"
    },
    {
      skillname: "VUEJS"
    },
    {
      skillname: "ANGULAR"
    },
    {
      skillname: "NESTJS"
    },
    {
      skillname: "NODEJS"
    },
    {
      skillname: "LARAVEL"
    },
    {
      skillname: "PYTHON"
    },
    {
      skillname: "REACT NATIVE"
    },
    {
      skillname: "FLUTTER"
    },
    {
      skillname: "AWS"
    },
    {
      skillname: "AGILE"
    },
    {
      skillname: "KANBAN"
    },
  ],
  portfolio: [
    // {
    //   name: "BRESWORK",
    //   description:
    //     "A mobile application for working mom and focus on breastfeeding.",
    //   imgurl: breswork,
    //   url: "",
    //   app: [
    //     {
    //       icon: "fa fa-android",
    //       url: "https://play.google.com/store/apps/details?id=com.breswork"
    //     },
    //     {
    //       icon: "fa fa-apple",
    //       url: "https://apps.apple.com/my/app/breswork/id1477468299"
    //     }
    //   ]
    // },
    {
      name: "AIA MALAYSIA",
      description:
        "A customer portal for AIAs' products.",
      imgurl: aia,
      url: "https://www.aia.com.my/",
      app: []
    },
    {
      name: "AIA Indonesia iRecruit",
      description:
        "An agent recruitment for AIA Indonesia",
      imgurl: aia,
      url: "https://www.aia-financial.co.id/en/agency/index.html",
      app: []
    },
    {
      name: "RHBNow Online Banking",
      description:
        "An internet banking system for RHB customer",
      imgurl: rhb,
      url: "https://onlinebanking.rhbgroup.com/my/login",
      app: []
    },
    {
      name: "RHB MyHome",
      description:
        "An app for RHB customer's mortgage application.",
      imgurl: myhome,
      url: "",
      app: [
        {
          icon: "fa fa-android",
          url: "https://play.google.com/store/apps/details?id=com.rhb_mortgage&hl=en"
        },
        {
          icon: "fa fa-apple",
          url: "https://itunes.apple.com/my/app/rhb-myhome/id1266226615?mt=8"
        }
      ]
    },
  ],
  testimonials: [
    {
      description: "This is a sample testimonial",
      name: "Some technical guy"
    },
    {
      description: "This is a sample testimonial",
      name: "Some technical guy"
    }
  ]
};

export default resumeData;
